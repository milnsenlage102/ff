package 方法;

public class Example {
    // 声明一个简单的方法
    public void sayHello() {
        System.out.println("Hello, World!");
    }
    
    // 带参数的方法
    public void greet(String name) {
        System.out.println("Hello, " + name + "!");
    }

    // 带返回值的方法
    public int add(int a, int b) {
        return a + b;
    }

    public static void main(String[] args) {
        Example example = new Example(); // 创建一个对象

        // 调用方法
        example.sayHello(); // 调用无参数方法
        example.greet("Alice"); // 调用带参数方法
        int result = example.add(3, 5); // 调用带返回值方法
        System.out.println("3 + 5 = " + result);
    }
}
