public class Car {
    // 属性
    String make;
    String model;
    int year;

    // 方法
    public void start() {
        System.out.println("The car is starting.");
    }

    public void accelerate() {
        System.out.println("The car is accelerating.");
    }

    public void brake() {
        System.out.println("The car is braking.");
    }

    public static void main(String[] args) {
        // 创建Car类的实例
        Car myCar = new Car();

        // 设置对象的属性
        myCar.make = "Toyota";
        myCar.model = "Camry";
        myCar.year = 2020;

        // 调用对象的方法
        myCar.start();
        myCar.accelerate();
        myCar.brake();
    }
}
