package 实验3;
import java.util.Scanner;
class Student1{
 String name;
 int age;
 String sex;
 public void set(String name,int age,String sex) {
	 this.name=name;
	 this.age=age;
	 this.sex=sex;
 }
 public void set(Student1 stu) {
	 this.set(stu.name,stu.age,stu.sex);
 }
 public Student1(String name,int age,String sex) {
	 this.set(name,age,sex);
 }
 public Student1() {
	 this("李华",18,"男");
 }
 public Student1(Student1 stu) {
	 this.set(stu);
 }
 public String toString(){
	 return "姓名:" +name+" 性别:"+sex+" 年龄:"+String.format("%02d",age);
 }
}
class Undergraduate extends Student1{
	String xueli;
	public void set(String xueli) {
		this.xueli=xueli;
	}
	public void set(Undergraduate stu) {
		super.set(stu.name,stu.age,stu.sex);
		this.xueli=stu.xueli;
	}
	public Undergraduate() {
		super();
		this.xueli="本科生";
	}
	public Undergraduate(String name,int age,String sex,String xueli) {
		super(name,age,sex);
		this.xueli=xueli;
	}
	public Undergraduate(Undergraduate  stu) {
		super.set(stu.name,stu.age,stu.sex);
		this.xueli=stu.xueli;
	}
	 public String toString(){
		 return "姓名:" +name+" 性别:"+sex+" 年龄:"+String.format("%02d",age)+" 学历:本科生";
	 }
}
class Postgraduate extends Student1{
	String xueli;
	public void set(String xueli) {
		this.xueli=xueli;
	}
	public void set(Postgraduate stu) {
		super.set(stu.name,stu.age,stu.sex);
		this.xueli=stu.xueli;
	}
	public Postgraduate() {
		super();
		this.xueli="研究生";
	}
	public Postgraduate(String name,int age,String sex,String xueli) {
		super(name,age,sex);
		this.set(xueli);
	}
	public Postgraduate(Postgraduate  stu) {
		super.set(stu.name,stu.age,stu.sex);
		this.xueli=stu.xueli;
	}
	 public String toString(){
		 return "姓名:" +name+" 性别:"+sex+" 年龄:"+String.format("%02d",age)+" 学历:研究生";
	 }
}
public class Student{
	public static void main(String[] args) {
	    Student1[] students = new Student1[100];
	    int i = 0;
	    Scanner scanner = new Scanner(System.in);
	    while (true) {
	        String name;
	        int age;
	        String sex;
	        String xueli;
	        System.out.println("请输入姓名、年龄、性别和学历（本科生输入1，研究生输入2 输入\"0\"结束）：");
	        name = scanner.next();
	        if (name.equals("0")) {
	            break;
	        }
	        age = scanner.nextInt();
	        sex = scanner.next();
	        xueli = scanner.next();
	        if(xueli.equals("1")) {
	        students[i] = new Undergraduate(name, age, sex, xueli);
	        }else if(xueli.equals("2")) {
	        	students[i]=new Postgraduate(name,age,sex,xueli);
	        }
	        i++;
	    }
	    scanner.close(); // 移动关闭操作到循环外
	    for (int j = 0; j < i; j++) {
	        System.out.println(students[j].toString());
	    }
	}

	 
 
}