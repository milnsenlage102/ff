package study;

public class MyDate {

	private int year,month,day;
	private static int thisYear ;//当前年份，私有静态成员变量
	static
	{
		thisYear =2023;//静态成员变量私有化 
	}
	public  MyDate (int year,int month, int day)// 构造方法
	{
		this.set(year,month,day);//调用类里的成员方法set
	}
	public MyDate() {//无参数的构造方法，指定缺省日期，重载
		this(1970,1,1);//调用本类中已声明的其他构造方法
	}
	public MyDate(MyDate date)//拷贝构造方法
	{
		this.set(date);
	}
	public void set (int year,int month,int day) {
		this.year=year;
		this.month=(month>=1&&month<=12)?month:1;
		this.day=(day>=1&&day<=31)?day:1;
	}
	public void set(MyDate date) { //重载
		this.set(date.year,date.month,date.day);//调用同名成员方法，不能使用this（）
}
	public int getYear() {
		return this.year;
	}
	public int getMonth() {
		return this.month;
	}
	public int getDay() {
		return this.day;
	}
	public String toString() {//字符串
		return year+"年"+String.format("%02d", month)+"月"+String.format("%02d", day)+"日";
	}
	public static int getThisYear() {
		return thisYear;       //获得今年年份，静态方法，访问静态成员变量
	}
	public static boolean isLeapYear(int year)//判断年份是否为闰年
	{
		return year%400==0||year%100!=0&&year%4==0;
	}
	public boolean isLeapYear(){//重载
		return isLeapYear(this.year);
	}
	public boolean equals(MyDate date) {
		//this 指代调用当前方法的对象
		return this==date||date!=null&&this.year==date.year&&
				this.month==date.month&&this.day==date.day;
		
	}
	public static int daysOfMonth(int year,int month)//返回指定年月的天数，静态方法
	{
		switch(month) {
		case 1:case 3: case 5 :case 7:case 8:case 10:case 12 :return 31;
		case 4:case 6: case 9:case 11:return 30;
		case 2:return MyDate.isLeapYear(year)?29:28;
		default :return 0;
		}
	}
	public int daysOfMonth() {
		//返回当月天数
		return daysOfMonth(this.year,this.month);
	}
	public void tomorrow() {
		this.day=this.day%this.daysOfMonth()+1;
		if(this.day==1)
		{
			this.month=this.month%12+1;
			if(this.month==1) {
				this.year++;
			}
		}
	}
	public MyDate yesterday() {//返回前一天日期
		MyDate date=new MyDate(this);
		date.day--;
		if(date.day==0) {
			date.day=31;
			date.month=(this.month-2+12)%12+1;
			if(date.month==12)
			{
				date.year--;
			}
		}
		
		return date;
	}
	class MyDate_ex
	{
		public static void main (String []args) {
			System.out.println
			("今年是"+MyDate.getThisYear()+",闰年？"+MyDate.isLeapYear(MyDate.getThisYear()));
		MyDate d1 =new MyDate(2018,8,31);
		System.out.println(d1.getYear()+"年,闰年？"+d1.isLeapYear()); 
		MyDate d2=new MyDate(d1);
		System.out.println("d1: "+d1+", d2："+d2+",d1==d2? "+(d1==d2)+ ",d1.equals(d2))?"
		+d1.equals(d2));
		System.out.print(d1+"的明天是");
		d1.tomorrow();
		System.out.println(d1+"\n"+d1+"的昨天是"+(d1=d1.yesterday()));
		}
			
	}
	
	

	
}